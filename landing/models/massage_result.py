from django.core.exceptions import ValidationError
from django.db import models


class MassageResult(models.Model):
    massage = models.ForeignKey(
        verbose_name='Массаж', to='landing.Massage', related_name='results',
        on_delete=models.CASCADE,
    )
    image = models.ImageField(
        verbose_name='Картинка', upload_to='massage_result_images', null=True,
        blank=True,
    )
    video_url = models.TextField(
        verbose_name='Ссылка на видео', null=True, blank=True,
    )
    position = models.PositiveIntegerField(verbose_name='Позиция', default=0)

    def clean(self):
        super().clean()
        if any([
            self.image and self.video_url,
            not self.image and not self.video_url
        ]):
            raise ValidationError('One of image and video url must be filled')

    class Meta:
        ordering = ['position']
