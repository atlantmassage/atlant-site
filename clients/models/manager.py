from django.db import models


class Manager(models.Model):
    name = models.CharField(verbose_name='Имя', max_length=100)
    email = models.EmailField(verbose_name='Email', max_length=100)
    is_active = models.BooleanField(verbose_name='Активен', default=True)

    def __str__(self):
        return self.name
