from django import forms

from landing.models import Testimonial


class TestimonialAdminForm(forms.ModelForm):
    class Meta:
        model = Testimonial
        fields = ['image', 'video_url']


class CourseTestimonialAdminForm(TestimonialAdminForm):
    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.related_with = Testimonial.RelatedWith.COURSES
        if commit:
            instance.save()
        return instance
