from dataclasses import dataclass

from django.urls import reverse_lazy
from django.views.generic import DetailView, TemplateView

from landing.helpers import get_neighbors
from landing.mixins import CountdownMixin, MenuItemsMixin
from landing.models import (
    Course, CourseTestimonial, Discount, Massage, Specialist, Testimonial,
)


@dataclass
class MenuItem:
    href: str
    text: str
    anchor: bool = True


class IndexView(CountdownMixin, MenuItemsMixin, TemplateView):
    countdown_target = Discount.Target.MASSAGES
    template_name = 'landing/index.html'
    menu_items = [
        MenuItem('#about-us', 'О нас'),
        MenuItem('#services', 'Услуги'),
        MenuItem('#testimonials', 'Отзывы'),
        MenuItem('#team', 'Наши специалисты'),
        MenuItem(reverse_lazy('landing:certificates'), 'Сертификаты', False),
        MenuItem(reverse_lazy('landing:courses'), 'Обучение', False),
        MenuItem('#contacts', 'Контакты'),
    ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'massages': Massage.objects.filter(is_active=True),
            'testimonials': Testimonial.objects.all(),
            'specialists': Specialist.objects.all(),
        })
        return context


class MassageDetailView(CountdownMixin, MenuItemsMixin, DetailView):
    countdown_target = Discount.Target.MASSAGES
    template_name = 'landing/massage_details.html'
    context_object_name = 'massage'
    menu_items = [
        MenuItem(reverse_lazy('landing:index'), 'На главную', False),
        MenuItem('#description', 'Описание'),
        MenuItem('#results', 'Наши работы'),
        MenuItem(reverse_lazy('landing:certificates'), 'Сертификаты', False),
        MenuItem(reverse_lazy('landing:courses'), 'Обучение', False),
        MenuItem('#contacts', 'Контакты'),
    ]

    def get_queryset(self):
        return Massage.objects.filter(is_active=True).prefetch_related(
            'results',
        )

    def get_context_data(self, **kwargs):
        return super().get_context_data(
            **kwargs, neighbors=get_neighbors(self.object),
        )


class CertificatesView(CountdownMixin, MenuItemsMixin, TemplateView):
    countdown_target = Discount.Target.CERTIFICATES
    template_name = 'landing/certificates.html'
    menu_items = [
        MenuItem(reverse_lazy('landing:index'), 'На главную', False),
        MenuItem('#certificates', 'Сертификаты'),
        MenuItem('#process', 'Процесс'),
        MenuItem(reverse_lazy('landing:courses'), 'Обучение', False),
        MenuItem('#contacts', 'Контакты'),
    ]


class CoursesView(CountdownMixin, MenuItemsMixin, TemplateView):
    countdown_target = Discount.Target.COURSES
    template_name = 'landing/courses.html'
    menu_items = [
        MenuItem(reverse_lazy('landing:index'), 'На главную', False),
        MenuItem('#description', 'О курсе'),
        MenuItem('#course-author', 'Об авторе'),
        MenuItem('#course-types', 'Варианты'),
        MenuItem('#testimonials', 'Отзывы'),
        MenuItem(reverse_lazy('landing:certificates'), 'Сертификаты', False),
        MenuItem('#contacts', 'Контакты'),
    ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'courses': Course.objects.filter(is_active=True),
            'testimonials': CourseTestimonial.objects.all(),
        })
        return context
