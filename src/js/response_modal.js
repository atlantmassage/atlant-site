const showModal = function (content, extra_button = null) {
  const textHtml = content.map(
    (text) => `<p class="response-modal__text">${text}</p>`,
  );

  const modal = $(`
    <div class="response-modal">
      <div class="response-modal__overlay"></div>
      <div class="response-modal__modal">
        <div class="response-modal__content">
          ${textHtml.join('')}
        </div>
        <div class="response-modal__button-wrap">
          ${extra_button ? extra_button : ''}
          <button class="response-modal__button response-modal__button--close">Закрыть</button>
        </div>
      </div>
    </div>
  `);

  const overlay = modal.find('.response-modal__overlay');

  const hideModal = function () {
    setTimeout(() => modal.remove(), 300);
    overlay.addClass('response-modal__overlay--leaving');
    modal
      .find('.response-modal__modal')
      .addClass('response-modal__modal--leaving');
  };

  overlay.on('click', hideModal);
  modal.find('.response-modal__button--close').on('click', hideModal);
  $('body').append(modal);
};
