from typing import List

from django.db.models import Model


def get_neighbors(instance: Model, **kwargs) -> List:
    objects = instance.__class__.objects
    prev = objects.filter(id__lt=instance.id, **kwargs).order_by('-id').first()
    next_ = objects.filter(id__gt=instance.id, **kwargs).order_by('id').first()
    neighbors = []

    if prev:
        neighbors.append(prev)

    if next_:
        neighbors.append(next_)

    return neighbors
