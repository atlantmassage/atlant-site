from django.urls import path

from clients.views import ClientRequestCreateView

app_name = 'clients'

urlpatterns = [
    path('create/', ClientRequestCreateView.as_view(), name='create'),
]
