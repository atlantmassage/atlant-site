from django.core.exceptions import ValidationError
from django.db import models


class TestimonialManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(related_with__isnull=True)


class Testimonial(models.Model):
    class RelatedWith(models.TextChoices):
        COURSES = 'courses', 'Курсы'

    image = models.ImageField(
        verbose_name='Картинка', upload_to='testimonial_images', null=True,
        blank=True,
    )
    video_url = models.TextField(
        verbose_name='Ссылка на видео', null=True, blank=True,
    )
    position = models.PositiveIntegerField(verbose_name='Позиция', default=0)
    related_with = models.CharField(
        max_length=100, blank=True, null=True, choices=RelatedWith.choices,
    )

    objects = TestimonialManager()

    def clean(self):
        super().clean()
        if any([
            self.image and self.video_url,
            not self.image and not self.video_url
        ]):
            raise ValidationError('One of image and video url must be filled')

    def __str__(self):
        return str(self.id)

    class Meta:
        ordering = ['position']
