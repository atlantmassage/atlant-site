'use strict';

const del = require('del');
const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');

const paths = {
  styles: {
    src: 'src/scss/**/*.scss',
    dest: 'dist/css',
  },
  scripts: {
    src: 'src/js/**/*.js',
    dest: 'dist/js',
  },
  images: {
    src: 'src/img/**/*',
    dest: 'dist/img',
  },
};

const clean = () => del('dist');

const compileStylesWithSourcemaps = () => {
  return gulp
      .src(paths.styles.src)
      .pipe(sourcemaps.init())
      .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
      .pipe(autoprefixer({cascade: false}))
      .pipe(sourcemaps.write())
      .pipe(rename('style.min.css'))
      .pipe(gulp.dest(paths.styles.dest));
};

const compileStyles = () => {
  return gulp
      .src(paths.styles.src)
      .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
      .pipe(autoprefixer({cascade: false}))
      .pipe(rename('style.min.css'))
      .pipe(gulp.dest(paths.styles.dest));
};

const compileScripts = () => {
  return gulp
      .src(paths.scripts.src)
      .pipe(uglify())
      .pipe(gulp.dest(paths.scripts.dest));
};

const minimizeImages = () => {
  return gulp
      .src(paths.images.src)
      .pipe(imagemin())
      .pipe(gulp.dest(paths.images.dest));
};

exports.build = gulp.series(clean, gulp.parallel(
  compileStyles,
  compileScripts,
  minimizeImages,
));

exports.watch = () => {
  (gulp.series(clean, gulp.parallel(
    compileStylesWithSourcemaps,
    compileScripts,
    minimizeImages,
  )))();
  gulp.watch(paths.styles.src, compileStylesWithSourcemaps);
  gulp.watch(paths.scripts.src, compileScripts);
  gulp.watch(paths.images.src, minimizeImages);
};
