from django.db import models


class ClientRequest(models.Model):
    name = models.CharField(verbose_name='Имя', max_length=100)
    phone_number = models.CharField(
        verbose_name='Номер телефона', max_length=100,
    )
    tag = models.CharField(
        verbose_name='Тэг', max_length=100, null=True, blank=True,
    )
    created_at = models.DateTimeField(
        verbose_name='Дата и время', auto_now_add=True,
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['created_at']
