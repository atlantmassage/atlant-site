from ckeditor.fields import RichTextField
from django.db import models
from django.urls import reverse


class Massage(models.Model):
    slug = models.SlugField(verbose_name='Путь в адресной строке')
    name = models.CharField(verbose_name='Название', max_length=100)
    short_description = models.TextField(verbose_name='Краткое описание')
    duration = models.CharField(verbose_name='Длительность', max_length=100)
    description = RichTextField(verbose_name='Описание')
    old_price = models.PositiveIntegerField(
        verbose_name='Стоимость без скидки',
    )
    price = models.PositiveIntegerField(verbose_name='Стоимость')
    is_active = models.BooleanField(verbose_name='Активен', default=True)
    thumbnail = models.ImageField(
        verbose_name='Картинка на карточке', upload_to='massage_thumbnails',
    )
    image = models.ImageField(
        verbose_name='Картинка на фоне', upload_to='massage_images',
    )
    position = models.PositiveIntegerField(verbose_name='Позиция', default=0)

    def get_page_url(self):
        return reverse('landing:massage_detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['position']
