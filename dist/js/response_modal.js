const showModal=function(o,s=null){const e=o.map(o=>`<p class="response-modal__text">${o}</p>`),l=$(`
    <div class="response-modal">
      <div class="response-modal__overlay"></div>
      <div class="response-modal__modal">
        <div class="response-modal__content">
          ${e.join("")}
        </div>
        <div class="response-modal__button-wrap">
          ${s||""}
          <button class="response-modal__button response-modal__button--close">Закрыть</button>
        </div>
      </div>
    </div>
  `),n=l.find(".response-modal__overlay");function a(){setTimeout(()=>l.remove(),300),n.addClass("response-modal__overlay--leaving"),l.find(".response-modal__modal").addClass("response-modal__modal--leaving")}n.on("click",a),l.find(".response-modal__button--close").on("click",a),$("body").append(l)};