$(function () {
  const countdown = $('#countdown');
  const days = $('#countdown-days');
  const hours = $('#countdown-hours');
  const minutes = $('#countdown-minutes');
  const seconds = $('#countdown-seconds');

  let secondsLeft = countdown.data('seconds-left');
  let intervalId;

  const secondsToObject = function (seconds) {
    const SECONDS_IN_MINUTE = 60;
    const SECONDS_IN_HOUR = 60 * SECONDS_IN_MINUTE;
    const SECONDS_IN_DAY = 24 * SECONDS_IN_HOUR;

    const days = Math.floor(seconds / SECONDS_IN_DAY);
    seconds -= days * SECONDS_IN_DAY;
    const hours = Math.floor(seconds / SECONDS_IN_HOUR);
    seconds -= hours * SECONDS_IN_HOUR;
    const minutes = Math.floor(seconds / SECONDS_IN_MINUTE);
    seconds -= minutes * SECONDS_IN_MINUTE;

    return { days, hours, minutes, seconds: Math.floor(seconds) };
  };

  const addZeros = function (value) {
    return value.toString().padStart(2, '0');
  };

  const updateCountdown = function () {
    const obj = secondsToObject(secondsLeft);

    days.text(addZeros(obj.days));
    hours.text(addZeros(obj.hours));
    minutes.text(addZeros(obj.minutes));
    seconds.text(addZeros(obj.seconds));

    secondsLeft--;

    if (secondsLeft <= 0) {
      clearInterval(intervalId);
    }
  };

  intervalId = setInterval(updateCountdown, 1000);
  updateCountdown();
});
