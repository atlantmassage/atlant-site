from django.db import models


class Specialist(models.Model):
    first_name = models.CharField(verbose_name='Имя', max_length=100)
    last_name = models.CharField(verbose_name='Фамилия', max_length=100)
    image = models.ImageField(
        verbose_name='Картинка', upload_to='specialist_images',
    )
    position = models.PositiveIntegerField(verbose_name='Позиция', default=0)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    class Meta:
        ordering = ['position']
