from django.http import HttpResponse, HttpResponseBadRequest
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from clients import utils
from clients.forms import ClientRequestForm


@method_decorator(csrf_exempt, name='dispatch')
class ClientRequestCreateView(View):
    def post(self, request, *args, **kwargs):
        form = ClientRequestForm(data=request.POST)

        if not form.is_valid():
            return HttpResponseBadRequest()

        form.instance.save()
        utils.notify_about_client_request(form.instance)
        return HttpResponse()
