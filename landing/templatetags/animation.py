from django import template

register = template.Library()


@register.filter
def delay_by_counter(index):
    DELTA_DELAY = 0.2

    return index * DELTA_DELAY
