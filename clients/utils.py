from django.conf import settings
from django.core.mail import send_mass_mail
from django.utils import timezone

from clients.models import Manager


def notify_about_client_request(client_request):
    datetime_format = '%H:%M:%S %d.%m.%Y'
    local_created_at = timezone.make_naive(client_request.created_at)
    text = f'''
        Клиент:

        Имя: {client_request.name}
        Номер телефона: {client_request.phone_number}
        Тэг: "{client_request.tag}"
        Дата отправки: {local_created_at.strftime(datetime_format)}
    '''
    _notify_managers('Новая заявка', text)


def _notify_managers(subject, text):
    if not (emails := _get_active_manager_emails()):
        return
    message = (subject, text, settings.EMAIL_HOST_USER, emails)
    send_mass_mail([message], fail_silently=True)


def _get_active_manager_emails():
    return list(
        Manager.objects.filter(is_active=True).values_list('email', flat=True),
    )
