$(function () {
  const menuButton = $('.menu-button');
  const menu = $('.mobile-menu');
  const menuItems = $('.mobile-menu__item');

  const toggleButton = function () {
    menuButton.toggleClass('menu-button--close');
  };

  const toggleMenu = function () {
    menu.toggleClass('mobile-menu--opened');
  };

  const toggleBodyScroll = function () {
    const body = $('body');

    if (body.css('overflow') === 'hidden') {
      body.css('overflow', 'visible');
    } else {
      body.css('overflow', 'hidden');
    }
  };

  menuButton.on('click', function () {
    toggleButton();
    toggleMenu();
    toggleBodyScroll();
  });

  menuItems.on('click', function () {
    toggleButton();
    toggleMenu();
    toggleBodyScroll();
  });
});
