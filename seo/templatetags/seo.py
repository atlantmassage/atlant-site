from django import template

from seo.models import Page

register = template.Library()


@register.inclusion_tag('seo/page_description.html')
def page_description(request_path):
    if not request_path.endswith('/'):
        request_path = request_path + '/'

    try:
        page = Page.objects.get(path=request_path)
        return {'page_description': page.description}
    except Page.DoesNotExist:
        pass
