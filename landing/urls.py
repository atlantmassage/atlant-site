from django.urls import path

from landing.views import (
    CertificatesView, CoursesView, IndexView, MassageDetailView,
)

app_name = 'landing'

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('certificates/', CertificatesView.as_view(), name='certificates'),
    path('courses/', CoursesView.as_view(), name='courses'),
    path('<slug:slug>/', MassageDetailView.as_view(),
         name='massage_detail'),
]
